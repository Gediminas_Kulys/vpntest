from django.db import models

# Create your models here.

class Login_forma(models.Model):
    vardas      =        models.CharField(max_length=125)
    description =        models.TextField(null=True)
    date_from   =        models.DateField(null=True)
    date_to     =        models.DateField(null=True)
    email       =        models.CharField(max_length=125)
