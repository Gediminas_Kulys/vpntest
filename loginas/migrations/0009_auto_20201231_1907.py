# Generated by Django 3.1.4 on 2020-12-31 17:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loginas', '0008_remove_login_form_pavarde'),
    ]

    operations = [
        migrations.AlterField(
            model_name='login_form',
            name='vardas',
            field=models.CharField(blank=True, max_length=125),
        ),
    ]
