# Generated by Django 3.1.4 on 2020-12-31 17:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loginas', '0014_auto_20201231_1934'),
    ]

    operations = [
        migrations.CreateModel(
            name='Login_form',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vardas', models.CharField(max_length=125)),
                ('description', models.TextField()),
                ('date_to', models.DateField()),
                ('date_from', models.DateField()),
            ],
        ),
        migrations.DeleteModel(
            name='vpn_form',
        ),
    ]
